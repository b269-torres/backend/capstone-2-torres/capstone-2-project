const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},


	orders : [ 

			{
				productName : {
					type : String,
			},
				quantity : {
					type : Number,
					default: 1
			},
			purchaseOn : {
				type : Date,
				default : new Date()
			},
			subtotal: {
				type : Number,
			}
			}
		],

	totalAmount : {
				type : Number,
			}
});


module.exports = mongoose.model("User", userSchema); 