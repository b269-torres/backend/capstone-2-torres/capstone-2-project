const Product = require("../models/Product");


module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			quantity : data.product.quantity
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// Product creation successful
			if (error) {
				return false;
			// Product creation failed
			} else {
				return true;
			};
		});
	
	} 
	// User is not an admin
	let message = Promise.resolve("User must be admin to access this");
	return message.then((value)=> {
		return {value};
	});
};


// Retrieve ALL Products
module.exports.getALLProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}

// Retrive ACTIVE courses
module.exports.getALLActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Update a course

module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error)=> {

		if (error) {
			return false;
		} else {
			return true;
		}
	})
}


// Archive a product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
