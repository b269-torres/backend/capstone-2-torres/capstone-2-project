const User = require("../models/User");
const Product = require("../models/Product");


const bcrypt = require("bcrypt");


const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email: reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0 ) {
			return true;
			// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return false
		}
	})
};




module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)


});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});

};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				// if password do not match
			}else {
				return false;
			};
		};
	});
};



module.exports.getProfile=(data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};



module.exports.orders = async (data) => {

let isUserUpdated = await User.findById(data.userId).then(async (user) => {
		const product = await Product.findById(data.productId);

         let totalAmount = 0;

        const items = {
            productId: data.productId,
            productName: product.name,
            quantity: data.quantity,
            purchasedOn: new Date(),
            subtotal: data.quantity * product.price,
            price: product.price
        };


        user.orders.push(items);

        const finalAmount = user.orders.reduce(
      (total, item) => total + items.subtotal,
      0
    );

    user.totalAmount = finalAmount;

		return user.save().then((user, error) => {
			if (error){
				return true;
			} else {
				return false;
			};
		});
	});
  
  
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({userId: data.userId});
		return product.save().then((product, error) => {
			if (error){
				return true;
			} else {
				return false;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return false;
	} else {
		return true;
	};
};

module.exports.activateUser = (reqParams) => {
	let updateActiveField = {
		isAdmin: true
	};
	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.getALLUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};