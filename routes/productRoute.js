const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");



router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the product
router.get("/allProducts", (req, res) => {
	productController.getALLProducts().then(resultFromController => res.send(resultFromController));
})

//route for retrieving active products
router.get("/active", (req, res) => {
	productController.getALLActiveProduct().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})


// Route for updating a product
router.put("/:productId", (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Route for archiving a product.
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;