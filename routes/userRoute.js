const express = require("express");

const router = express.Router(); 

const userController = require("../controllers/userController");


const auth = require("../auth")

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(
		resultFromController => res.send(resultFromController));
})



//Route for authenticated user orders
router.post("/orders", (req, res) => {

	let data = {
		userId: req.body.id, 
			productId: req.body.productId,
			quantity: req.body.quantity
	}
	userController.orders(req.body).then(
		resultFromController => res.send(resultFromController));
})


router.patch("/:userId/activate", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.activateUser(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/allUsers", (req, res) => {
	userController.getALLUsers().then(resultFromController => res.send(resultFromController));
})

module.exports = router;