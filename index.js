// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");


// This allows us to use all the routes defined in "productRoute.js"
const productRoute = require("./routes/productRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json())
app.use(express.urlencoded({extended:true}));




// Allows all the user routes created in "userRoutes.js" file to use "/users" as route (resource)
// localhost:3000/users
app.use("/users", userRoute);

//Allows us to create product with the admin
app.use("/products", productRoute);

// Database Connection 
mongoose.connect("mongodb+srv://reybutchtorres:admin123@zuitt-bootcamp.p8hs8ti.mongodb.net/Capstone2Project?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});



mongoose.connection.once("open", () => console.log('Now connected to the cloud database'));



app.listen(process.env.PORT || 3000, () => console.log(`Now connected to port ${process.env.PORT || 3000}`) )